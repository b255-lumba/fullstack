import { Fragment } from 'react';
import { Container } from 'react-bootstrap';
import React from 'react';
import AppNavBar from './components/AppNavBar';
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';
import Home from './pages/Home';
import './App.css';

function App() {
  return (
    <React.Fragment>
      <AppNavBar />
      <Container>
        <Home />
      </Container>
    </React.Fragment>
  );
}

export default App;
